class Deck < ApplicationRecord
  belongs_to :user
  has_many :cards, dependent: :destroy
  validates :name, presence: true, length: {minimum: 5}
  validates_uniqueness_of :name , :scope => [:user_id],
      :case_sensitive => false,
      :message => "Você já possui um deck de mesmo nome"
end
