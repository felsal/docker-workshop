class CardsController < ApplicationController
    
    before_action :authenticate_user!

    def create
        @deck = Deck.find(params[:deck_id])
        @card = @deck.cards.create(card_params)
        if @card.save
            json_response @card 
        else
            json_response @card.errors 
        end
    end

    def update
        @deck = Deck.find(params[:deck_id])
        @card = @deck.cards.find(params[:id])
        if @card.update(card_params)
            json_response @card
        else
            json_response @card.errors
        end
    end

    def destroy
        @deck = Deck.find(params[:deck_id])
        @card = @deck.cards.find(params[:id])
        @card.destroy
        #redirect_to @deck # o que fazer depois disso
    end

    def show
        @deck = Deck.find(params[:deck_id])
        @card = @deck.cards.find(params[:id])
        json_response @card
    end
    def index
        @deck = Deck.find(params[:deck_id])
        print "param #{params}"
        @cards = @deck.cards.all
        json_response @cards
    end
    private
    def card_params
        params.permit(:front, :back)
    end
end
